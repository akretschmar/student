
package mcvdemo;


public class StudentView {
    public void printStudentDetails(String name, String rollNo, String id){
        System.out.println("--Student--");
        System.out.println("Name: " + name);
        System.out.println("Roll No: " + rollNo);
        System.out.println("ID: " + id);
    }
}
