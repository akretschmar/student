
package mcvdemo;


public class StudentTest {

    
    public static void main(String[] args) {
        Student model = retrieveStudentFromDatabase();
        StudentView view = new StudentView();
        
        StudentController controller = new StudentController(model, view);
        
        controller.updateView();
    }
    
    private static Student retrieveStudentFromDatabase(){
        Student student = new Student();
        student.setName("Ashley");
        student.setRollNo("4");
        student.setId("991504714");
        return student;
    }
    
}
