
package mcvdemo;


public class StudentController {
    private StudentView view;
    private Student model;
    
    public StudentController(Student model, StudentView view){
        this.model = model;
        this.view = view;
    }
    
    
    public String getStudentName(){
        return model.getName();
    }
    public String getStudentRollNo(){
      return model.getRollNo();
    }
    public String getStudentId(){
        return model.getId();
    }
    
    public void setStudentName(String name){
        model.setName(name);
    }
    public void setStudentRollNo(String rollNo){
        model.setRollNo(rollNo);
    }
    public void setStudentId(String id){
        model.setId(id);
    }
    public void updateView(){
        view.printStudentDetails(model.getName(), model.getRollNo(), model.getId());
    }
}
