package mcvdemo;


public class Student {
    private String name;
    private String rollNo;
    private String id;
    
    public String getName(){
        return name;
    }
    
    public String getRollNo(){
        return rollNo;
    }
    
    public String getId(){
        return id;
    }
    
    public void setName(String name){
        this.name = name;
    }
    public void setRollNo(String rollNo){
        this.rollNo = rollNo;
    }
    public void setId(String id){
        this.id = id;
    }
    
}
